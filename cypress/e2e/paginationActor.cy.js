import "../support/commands";

describe("PaginationComponent", () => {
  it("should navigate to different pages", () => {
    cy.visit("/actors");
    cy.wait(3000);
    cy.get(".MuiPagination-root")
      .find("button.MuiPaginationItem-page")
      .should("contain", "1");

    const targetPage = 3;
    cy.get(".MuiPagination-root")
      .find("li button.MuiPaginationItem-page")
      .eq(targetPage - 1)
      .click();

    // Verify that the page has changed
    cy.get(".MuiPagination-root")
      .should("contain", `${targetPage}`);
  });

  it("should navigate to page 5", () => {
    cy.visit("/actors");

    const targetPage = 5;
    cy.get(".MuiPagination-root")
      .find("li button.MuiPaginationItem-page")
      .eq(targetPage - 1)
      .click();

    // Verify that the page has changed
    cy.get(".MuiPagination-root")
      .should("contain", `${targetPage}`);
  });



    it("should show an error message for exceeding page limit", () => {
      cy.visit("/actors");
      cy.get(".MuiPagination-root")
      // Click on the last page using the custom command
      cy.clickSecondLastPage();
    
      // Verify that the error message is displayed
      cy.wait(3000);
      cy.wait(3000);
      cy.get("h1")
        .should("contain", "Invalid page number. Page must be between 1 and 1000.")
        .then(() => {
          // Find and click the "Go to Home" button and redirect to page 1 again
          cy.get("button").contains("Go Back").click();
        });
    
      // Verify that the pagination is reset to the initial page
      cy.get(".MuiPagination-root")
        .find("button.MuiPaginationItem-page")
        .should("contain", "1");
    });
  });
    