describe("Reading TV Series Reviews", () => {
    let selectedSeries;
    let response;
  
    before(() => {
      // Request the list of TV series
      cy.request(
        `https://api.themoviedb.org/3/discover/tv?api_key=${Cypress.env(
          "TMDB_KEY"
        )}&language=en-US&include_adult=false&include_video=false&page=1`
      )
      .its("body")
      .then((res) => {
        response = res;
        // Find a series with reviews
        selectedSeries = response.results.find((series) => series.vote_count > 0);
      });
    });
  
    it("navigates to the TV Serie details page and displays reviews", () => {
      // If no series with reviews is found, skip the test
      if (!selectedSeries) {
        cy.log("No TV Series with reviews found. Skipping the test.");
        return;
      }
  
      cy.visit(`/series/${selectedSeries.id}`); // Visit Series Details Page
  
      // Click the 'Reviews' button
      cy.contains("Reviews").click();
  
      // Verify that the reviews table is displayed
      cy.get(".MuiTableContainer-root").should("exist");
  
      // Use cy.wrap() to pass response into the then block
      cy.wrap(response).then(() => {
        cy.get(".MuiTableRow-root").then(($rows) => {
          if ($rows.length <= 1) {
            // No reviews found for the current series, try another one
            cy.log("No reviews available for the current TV Series. Trying another one.");
  
            // Retry with the specific series ID known to have reviews
            selectedSeries = response.results.find((series) => series.id === 84958);
  
            // If still no series with reviews is found, skip the test
            if (!selectedSeries) {
              cy.log("No more TV Series with reviews found. Skipping the test.");
              return;
            }
  
            cy.visit("/series");
            cy.visit(`/series/${selectedSeries.id}`);
            cy.contains("Reviews").click();
  
            // Verify that the reviews table is displayed
            cy.get(".MuiTableContainer-root").should("exist");
  
            // Verify that there is at least one review in the table
            cy.get(".MuiTableRow-root").should("have.length.greaterThan", 0);
  
            // Add more assertions based on your specific HTML structure
  
            // Click the 'Full Review' link for the first review
            cy.contains("Full Review").first().click();
  
            // Verify that the URL includes the correct review ID
            cy.url().should("include", "series/reviews/").and("match", /\/reviews\/\w+/);
          }
        });
      });
    });
  });
  