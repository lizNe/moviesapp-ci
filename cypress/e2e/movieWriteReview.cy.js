let movies;

describe("The movie review feature", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/discover/movie?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        movies = response.results;
      });
  });

  beforeEach(() => {
    // Start from the homepage
    cy.visit("/");
    // Select two favorites and navigate to Favourites page
    cy.get("button[aria-label='add to favorites']").eq(1).click();
    cy.get(".navbar-nav").contains("More").click();
    cy.contains("Movie Favorites").click();
    // Click the "Write Movie Review" button
    cy.get(".MuiCard-root").eq(0).find("[data-testid='RateReviewIcon']").click();
    // Wait for a brief moment to ensure navigation has occurred
    cy.wait(1000);
  });

  it("writes a review with a name and content shorter than 10 letters", () => {
    // Input a name in the author field
    cy.get("input[name='author']").clear().type("John");

    cy.get("textarea[name='review']").clear().type("Rubbish");

    cy.get(".MuiSelect-select").click();
    cy.contains("Terrible").click();
    cy.get("button[type='submit']").click();

 // Assert that an error message appears indicating the review is too short
 cy.get(".MuiAlert-message").should("contain", "Review is too short");

 cy.get("textarea[name='review']").clear().type("It was more boring and rubbish than I thought");

cy.get("button[type='submit']").click();
cy.contains("Thank you for submitting a review");
 // Close the success notification
 cy.get(".MuiSnackbar-root").find("button[aria-label='Close']").click();

// After closing the notification, verify that you are on the Movie Favorites page
cy.url().should("include", "/movies/favorites");

// Also, verify that the two favorites are still present
cy.get("button[aria-label='remove from favorites']").should("have.length", 1);

});
});
