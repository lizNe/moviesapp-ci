import "../support/commands"; // Adjust the path based on the actual structure


let series;

describe("Navigation", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/discover/tv?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        series = response.results;
      });
  });
  beforeEach(() => {
    cy.visit("/series");
  });
  describe("From the Series Home page to a Serie's details", () => {
    it("navigates to the series details page and change browser URL", () => {
      cy.navigateToSeriesDetails(0);
      cy.url().should("include", `/series/${series[0].id}`);
    });
  });
  describe("Using the site header", () => {
    describe("when the viewport is desktop scale", () => {
      it("navigate via the button links", () => {
        cy.get(".navbar-nav").contains("More").click();
        cy.contains("TV Show Favorites").click();
        cy.url().should("include", `/series/favorites`);
        cy.get(".navbar-nav").contains("TV Series").click();
        cy.url().should("include", `/series`);
      });
    });
    describe(
      "when the viewport is mobile scale",
      {
        viewportHeight: 896,
        viewportWidth: 414,
      },
      () => {
        it("navigate via the dropdown menu", () => {
          cy.get(".navbar-toggler").click();
          cy.get("li").contains('TV Show Favorites').click();
          cy.url().should("include", `/series/favorites`);
        
          cy.get(".navbar-toggler").click();
          cy.get("li").contains('TV Series').click();
          cy.url().should("include", `/series`);
        });
      }
    );
  });

  describe("From the favorites series page to a series details", () => {
    beforeEach(() => {
        // Select two favourites and navigate to Series Favourites page
        cy.get("button[aria-label='add to favorites']").eq(1).click();
        cy.get("button[aria-label='add to favorites']").eq(3).click();
        cy.navigateToSeriesFavorites();

      });
  
    it("navigates to a series details from the favorite series page", () => {
      cy.navigateToSeriesFavorites();

  
      // Check if there are any favorited series on the page
      cy.get(".MuiCard-root").should("have.length.gt", 0);
  
      // Click the "More Info" button for the first favorited mseries
      cy.navigateToSeriesDetails(0);
  
      // Verify that you are on the series details page
      cy.url().should("include", `/series/`); // Verify the URL structure for movie details
  
  });
});
  
  
  describe("The forward/backward links", () => {
    beforeEach(() => {
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
    });
    it("navigates between the series detail page and the Serie Home page.", () => {
      cy.get("svg[data-testid='ArrowBackIcon'").click();
      cy.url().should("not.include", `/series/${series[0].id}`);
      cy.get("svg[data-testid='ArrowForwardIcon'").click();
      cy.url().should("include", `/series/${series[0].id}`);
    });
  });

  
});