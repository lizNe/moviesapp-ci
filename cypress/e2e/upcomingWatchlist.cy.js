let movies;

describe("The watchlist feature", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/movie/upcoming?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        movies = response.results;
      });
  });

  beforeEach(() => {
    cy.visit("/movies/upcoming");
  });

  describe("Selecting Upcoming Movies to Watchlist", () => {
    it("selected movie card shows the notification", () => {
      // Click the "add to watchlist" button
      cy.get("button[aria-label='add to watchlist']").eq(1).click();

      // Wait for the notification to appear
      cy.get(".MuiAlert-message").should("contain", "Movie added to watchlist");
      // Verify that the notification disappears
      cy.get(".MuiAlert-message").should("not.exist");
    });
  });

  describe("The Movie Watchlist Page", () => {
    beforeEach(() => {
      // Select two movies for the watchlist
      cy.get("button[aria-label='add to watchlist']").eq(1).click();
      cy.get("button[aria-label='add to watchlist']").eq(3).click();

      // Navigate to the watchlist page
      cy.get(".navbar-nav").contains("More").click();
      cy.contains("Watchlist").click();
    });

    it("only the tagged movies are listed", () => {
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", 2);

      cy.get('p.MuiTypography-root.MuiTypography-subtitle1')
        .eq(0)
        .should("contain", movies[1].title);

      cy.get('p.MuiTypography-root.MuiTypography-subtitle1')
        .eq(1)
        .should("contain", movies[3].title);
    });

    it("removes deleted movies from watchlist", () => {
      cy.get('.MuiCardActions-root')
        .eq(0)
        .within(() => {
          cy.get("button[aria-label='remove from watchlist']").click();
        });
    
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", 1);
    
      cy.get('.MuiCardActions-root')
        .eq(0)
        .within(() => {
          cy.get("button[aria-label='remove from watchlist']").click();
        });
    
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("not.exist");
    });
    
  });
});
