describe("Reading Movie Reviews", () => {
    let selectedMovie;
  
    before(() => {
      cy.request(
        `https://api.themoviedb.org/3/movie/popular?api_key=${Cypress.env(
          "TMDB_KEY"
        )}&language=en-US&include_adult=false&include_video=false&page=1`
      )
        .its("body")
        .then((response) => {
          selectedMovie = response.results[0];
        });
    });
  
    it("navigates to the movie details page and displays reviews", () => {
      // Click "More Info" on the first movie card
      cy.visit("/"); // Visit Movies Home Page
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
  
      // Verify the URL includes the correct movie ID
      cy.url().should("include", `/movies/${selectedMovie.id}`);
      cy.wait(3000);
      
      // Click the 'Reviews' button
      cy.contains("Reviews").click();
  
      // Verify that the reviews table is displayed
      cy.get(".MuiTableContainer-root").should("exist");
  
      // Verify that there is at least one review in the table
      cy.get(".MuiTableRow-root").should("have.length.greaterThan", 0);
  
      // Add more assertions based on your specific HTML structure
  
      // Click the 'Full Review' link for the first review
      cy.contains("Full Review").first().click();
  
      // Verify that the URL includes the correct review ID
      cy.url().should("include", "/reviews/").and("match", /\/reviews\/\w+/);
  
      // Add more assertions based on your specific HTML structure
    });
    
  });
  