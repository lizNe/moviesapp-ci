import "../support/commands"; 

let movies;

describe("Navigation Upcoming page", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/movie/upcoming?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        movies = response.results;
      });
  });
  beforeEach(() => {
    cy.visit("/movies/upcoming");
  });
  describe("From the Upcoming Movies Page to a Movies details", () => {
    it("navigates to the movie details page and change browser URL", () => {
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
      cy.url().should("include", `/movies/${movies[0].id}`);
    });
  });
  describe("Using the site header", () => {
    describe("when the viewport is desktop scale", () => {
      it("navigate via the button links", () => {

        cy.navigateToWatchlistDesktop();
        
        cy.url().should("include", `/movies/upcoming/watchlist`);

        cy.navigateToUpcomingMoviesDesktop();

        cy.url().should("include", `/movies/upcoming`);
      });
    });
    describe(
      "when the viewport is mobile scale",
      {
        viewportHeight: 896,
        viewportWidth: 414,
      },
      () => {
        it("navigate via the dropdown menu", () => {
          cy.navigateToWatchlistMobile();

          cy.url().should("include", `/movies/upcoming/watchlist`);
        
          cy.navigateToUpcomingMoviesMobile();

          cy.url().should("include", `/movies/upcoming`);
        });
      }
    );
  });

  describe("From the Watchlist page to a Movie details", () => {
    beforeEach(() => {
        // Select two movies for Watchlist and navigate to Watchlist Page
        cy.wait(3000);
        cy.get("button[aria-label='add to watchlist']").eq(1).should('be.visible').click();
        cy.get("button[aria-label='add to watchlist']").eq(3).should('be.visible').click();
        cy.get(".navbar-nav").contains("More").click();
        cy.contains("Watchlist").click();
      });
  
    it("navigates to a movie details from the Movie Watchlist Page", () => {
      cy.navigateToWatchlistDesktop();
  
      // Check if there are any movies watchlisted on the page
      cy.get(".MuiCard-root").should("have.length.gt", 0);
  
      // Click the "More Info" button for the first favorited mseries
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
  
      // Verify that you are on the movie details page
      cy.url().should("include", `/movies/`); // Verify the URL structure for movie details
  
  });
});
  
  
  describe("The forward/backward links", () => {
    beforeEach(() => {
      cy.get(".MuiCardActions-root").eq(0).contains("More Info").click();
    });
    it("navigates between the Movies detail page and the Upcoming Movies page.", () => {
      cy.get("svg[data-testid='ArrowBackIcon'").click();
      cy.url().should("not.include", `/movies/${movies[0].id}`);
      cy.get("svg[data-testid='ArrowForwardIcon'").click();
      cy.url().should("include", `/movies/${movies[0].id}`);
    });
  });

  
});