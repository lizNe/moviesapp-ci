import "../support/commands"; 

let actors;

describe("Navigation for Actors Page", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/person/popular?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        actors = response.results;
      });
  });

  beforeEach(() => {
    cy.visit("/actors");
  });

  describe("From the Actors page to an Actor's details", () => {
    it("navigates to the actor details page and changes browser URL", () => {
      cy.navigateToActorDetails(0);
      cy.url().should("include", `/actors/${actors[0].id}`);
      cy.get(".MuiButtonBase-root.MuiIconButton-root.MuiIconButton-sizeMedium[aria-label='go back']").click();
      cy.url().should("include", "/actors");

    });
  });
  describe("Using the site header", () => {
    describe("when the viewport is desktop scale", () => {
      it("navigate via the button links", () => {
        cy.navigateToPopularActorsDesktop();
        cy.url().should("include", `/actors`);
      });
    });
    describe(
      "when the viewport is mobile scale",
      {
        viewportHeight: 896,
        viewportWidth: 414,
      },
      () => {
        it("navigate via the dropdown menu", () => {
          cy.navigateToPopularActorsMobile();
          cy.url().should("include", `/actors`);
        });
      }
    );
  });
  describe("The forward/backward links", () => {
    beforeEach(() => {
      cy.get(".MuiCardContent-root").eq(0).contains("More Info").click();
    });
    it("navigates between the Actor detail page and the Actors Home page.", () => {
      cy.get("svg[data-testid='ArrowBackIcon'").click();
      cy.url().should("not.include", `/actors/${actors[0].id}`);
      cy.get("svg[data-testid='ArrowForwardIcon'").click();
      cy.url().should("include", `/actors/${actors[0].id}`);
    });
  });


});
