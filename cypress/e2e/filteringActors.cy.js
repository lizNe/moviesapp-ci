import { filterByActorName } from "../support/e2e";

let actors; // List of actors from TMDB

describe("Filtering Actors", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/person/popular?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&page=1`
    )
      .its("body")
      .then((response) => {
        actors = response.results;
      });
  });

  beforeEach(() => {
    cy.visit("/actors"); 
  });

  describe("By actor name", () => {
    it("only display actors with 'a' in the name", () => {
      const searchString = "a";
      const matchingActors = filterByActorName(actors, searchString);
  
      cy.get("#filled-search").clear().type(searchString);
  
      // Intercept any uncaught exceptions during the test
      cy.on('uncaught:exception', (err, runnable) => {
        // Check if the error message contains the expected text
        if (err.message.includes(`No actors found with the name '${searchString}'`)) {
          // If the expected error occurred, we can continue the test
          return true;
        }
        // If it's not the expected error, fail the test
        throw err;
      });
  
      // Modify the selector to target the actor name element
      cy.get('.MuiTypography-root.MuiTypography-h5').should("have.length", matchingActors.length);
  
      cy.get('.MuiTypography-root.MuiTypography-h5').each(($actorName, index) => {
        cy.wrap($actorName).should("have.text", matchingActors[index].name);
      });
    });
  
    it("handles case when there are no matches", () => {
      const searchString = "xyxxzyyzz";
  
      cy.get("#filled-search").clear().type(searchString);
  
      // Intercept any uncaught exceptions during the test
      cy.on('uncaught:exception', (err, runnable) => {
        // Check if the error message contains the text
        if (err.message.includes(`No actors found with the name '${searchString}'`)) {
          // If the expected error occurred, continue the test
          return true;
        }
  
        // If it's not the expected error, fail the test
        throw err;
      });
  
      // Ensure no actors are displayed
      cy.get('.MuiTypography-root.MuiTypography-h5').should("not.exist");
    });
  });
});
  