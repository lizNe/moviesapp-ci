import { filterBySerieGenre, filterBySerieTitle } from "../support/e2e";

let series; // List of Discover series from TMDB

describe("Filtering Series", () => {
  before(() => {
    // Get series from TMDB and store them locally.
    cy.request(
      `https://api.themoviedb.org/3/discover/tv?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        series = response.results;
      });
  });

  beforeEach(() => {
    cy.visit("/series");
  });

  describe("By series title", () => {
    it("only display series with 's' in the title", () => {
      const searchString = "s";
      const matchingSeries = filterBySerieTitle(series, searchString);
      cy.get("#filled-search").clear().type(searchString); // Enter 's' in the text box
  
      // Modify the selector to target the series name element
      cy.get('.MuiTypography-subtitle1').should(
        "have.length",
        matchingSeries.length
      );
  
      cy.get('.MuiTypography-subtitle1').each(($card, index) => {
        cy.wrap($card).should("have.text", matchingSeries[index].name);
      });
    });

    it("handles case when there are no matches", () => {
      const searchString = "xyxxzyyzz";
      cy.get("#filled-search").clear().type(searchString); // Enter s in text box
      cy.get('.MuiTypography-subtitle1').should("have.length", 0);
    });
  });

  describe("By series genre", () => {
    it("show series with the selected genre", () => {
      const selectedGenreId = 18;
      const selectedGenreText = "Drama";
      const matchingSeries = filterBySerieGenre(series, selectedGenreId);
      cy.get("#genre-select").click();
      cy.get("li").contains(selectedGenreText).click();
  
      // Modify the selector to target the <p> element within .MuiTypography-subtitle1
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", matchingSeries.length);
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').each(($pElement, index) => {
        cy.wrap($pElement).contains(matchingSeries[index].name);
      });
    });
  });
  

  describe("Combined genre and title for series", () => {
    it("displays series with the selected genre and 's' in the title", () => {
      const selectedGenreId = 18; // Example: Drama genre
      const selectedGenreText = "Drama"; // Example: Genre text
      const titleSearchString = "s"; // Example: Title filter with 's'

      // Filter series by genre and title
      const genreFilteredSeries = filterBySerieGenre(series, selectedGenreId);
      const titleFilteredSeries = filterBySerieTitle(series, titleSearchString);
      const combinedFilteredSeries = series.filter((serie) => {
        return (
          genreFilteredSeries.some((gs) => gs.id === serie.id) &&
          titleFilteredSeries.some((ts) => ts.id === serie.id)
        );
      });

      // Perform actions in the UI
      cy.get("#genre-select").click();
      cy.get("li").contains(selectedGenreText).click();
      cy.get("#filled-search").clear().type(titleSearchString);

      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", combinedFilteredSeries.length);
      cy.get('p.MuiTypography-root.MuiTypography-subtitle1').each(($pElement, index) => {
        cy.wrap($pElement).contains(combinedFilteredSeries[index].name);
      });
    });
  });
});
