// let series;

// describe("The favourites feature", () => {
//   before(() => {
//     cy.request(
//       `https://api.themoviedb.org/3/discover/tv?api_key=${Cypress.env(
//         "TMDB_KEY"
//       )}&language=en-US&include_adult=false&include_video=false&page=1`
//     )
//       .its("body")
//       .then((response) => {
//         series = response.results;
//       });
//   });
//   beforeEach(() => {
//     cy.visit("/series");
//   });

//   describe("Selecting TV Series favourites", () => {
//     it("selected serie card shows the red heart", () => {
//       cy.get(".MuiCardMedia-root").eq(1).find("svg").should("not.exist");
//       cy.get("button[aria-label='add to favorites']").eq(1).click();
//       cy.get(".MuiCardMedia-root")
//       cy.get(".MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-1scgrso-MuiSvgIcon-root").should("exist");
//     });
//   });


//   describe("The TV Series favourites page", () => {
//     beforeEach(() => {
//       // Select two favourites and navigate to Favourites page
//       cy.get("button[aria-label='add to favorites']").eq(1).click();
//       cy.get("button[aria-label='add to favorites']").eq(3).click();

//       cy.get(".navbar-nav").contains("More").click();
//       cy.contains("TV Show Favorites").click();
//     });
//     it("only the tagged series are listed", () => {
//       cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", 2);
    
//       cy.get('p.MuiTypography-root.MuiTypography-subtitle1')
//         .eq(0)
//         .should("contain", series[1].name);
    
//       cy.get('p.MuiTypography-root.MuiTypography-subtitle1')
//         .eq(1)
//         .should("contain", series[3].name);
//     });
//     it("removes deleted series", () => {
//       // Assume you want to remove both series in the favorites list
      
//       // Remove the first series
//       cy.get('.MuiCardActions-root')
//         .eq(0)
//         .within(() => {
//           cy.get("button[aria-label='remove from favorites']").click();
//         });
    
//       // Check if the first series is no longer listed
//       cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("have.length", 1);
    
//       // Remove the second series
//       cy.get('.MuiCardActions-root')
//         .eq(0)
//         .within(() => {
//           cy.get("button[aria-label='remove from favorites']").click();
//         });
    
//       // Check if both series are now removed
//       cy.get('p.MuiTypography-root.MuiTypography-subtitle1').should("not.exist");
//     });
    
    
//   });
// });