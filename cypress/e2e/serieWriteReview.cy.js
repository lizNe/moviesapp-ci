let series;

describe("The TV Series review feature", () => {
  before(() => {
    cy.request(
      `https://api.themoviedb.org/3/discover/tv?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")
      .then((response) => {
        series = response.results;
      });
  });

  beforeEach(() => {
    // Start from the homepage
    cy.visit("/series");
    // Select two favorites and navigate to Favourites page
    cy.get("button[aria-label='add to favorites']").eq(1).click();
    cy.get("button[aria-label='add to favorites']").eq(3).click();
    cy.get(".navbar-nav").contains("More").click();
    cy.contains("TV Show Favorites").click();
    // Click the "Write Movie Review" button
    cy.get(".MuiCard-root").eq(0).find("[data-testid='RateReviewIcon']").click();
    // Wait for a brief moment to ensure navigation has occurred
    cy.wait(1000);
  });

  it("writes a review with a name and content shorter than 10 letters", () => {
    // Input a name in the author field
    cy.get("input[name='author']").clear().type("Alison");

    cy.get("textarea[name='review']").clear().type("Bad Show");

    cy.get(".MuiSelect-select").click();
    cy.contains("Poor").click();
    cy.get("button[type='submit']").click();

 // Assert that an error message appears indicating the review is too short
 cy.get(".MuiAlert-message").should("contain", "Review is too short");

 cy.get("textarea[name='review']").clear().type("Such a bad show. I will never watch it again!");

cy.get("button[type='submit']").click();
cy.contains("Thank you for submitting a review");
 // Close the success notification
 cy.get(".MuiSnackbar-root").find("button[aria-label='Close']").click();

// After closing the notification, verify that you are on the Movie Favorites page
cy.url().should("include", "/series/favorites");

// Also, verify that the two favorites are still present
cy.get("button[aria-label='remove from favorites']").should("have.length", 2);

// Optionally, you can check the titles of the favorites
cy.get(".MuiTypography-root.MuiTypography-subtitle1").eq(0).should("contain", series[1].name);
cy.get(".MuiTypography-root.MuiTypography-subtitle1").eq(1).should("contain", series[3].name);
});
});
