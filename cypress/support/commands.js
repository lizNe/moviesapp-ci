
//Pagination Custom Commands
Cypress.Commands.add("clickSecondLastPage", () => {
    const paginationSelector = ".MuiPagination-root";
  
    cy.get(`${paginationSelector} li:nth-last-child(2) button.MuiPaginationItem-page`)
      .click(); 
  });

  //Series Naviagtion Commands
  Cypress.Commands.add("navigateToSeriesDetails", (index) => {
    cy.get(".MuiCardActions-root").eq(index).contains("More Info").click();
  });
  
  Cypress.Commands.add("navigateToSeriesFavorites", () => {
    cy.get(".navbar-nav").contains("More").click();
    cy.contains("TV Show Favorites").click();
  });
  
  //Actors Navigation Commands
  Cypress.Commands.add("navigateToActorDetails", (index) => {
    cy.get(".MuiButtonBase-root.MuiButton-root.MuiButton-outlined").eq(index).click();
  });
  
  //Mobile Version
  Cypress.Commands.add("navigateToPopularActorsMobile", () => {
    cy.get(".navbar-toggler").click();
    cy.get("li").contains('Popular People').click();
  });
  
  //Desktop Version
  Cypress.Commands.add("navigateToPopularActorsDesktop", () => {
    cy.get(".navbar-nav").contains("Popular People").should('be.visible').click();
  });
  

  //Upcoming Navigation Commands
  Cypress.Commands.add("navigateToWatchlistMobile", () => {
    cy.get(".navbar-toggler").click();
    cy.get("li").contains('Watchlist').click();
  });
  
  Cypress.Commands.add("navigateToUpcomingMoviesMobile", () => {
    cy.get(".navbar-toggler").click();
    cy.get("li").contains('Upcoming').click();
  });
  
  //Desktop
  Cypress.Commands.add("navigateToWatchlistDesktop", () => {
    cy.get(".navbar-nav").contains("More").click();
    cy.contains("Watchlist").click();  });
  
  Cypress.Commands.add("navigateToUpcomingMoviesDesktop", () => {
    cy.get(".navbar-nav").contains("Upcoming").should('be.visible').click();
  });
  