export const filterByTitle = (movieList, string) =>
  movieList.filter((m) => m.title.toLowerCase().search(string) !== -1);

export const filterByGenre = (movieList, genreId) =>
  movieList.filter((m) => m.genre_ids.includes(genreId));

  export const filterBySerieTitle = (seriesList, string) =>
  seriesList.filter((s) => s.name.toLowerCase().search(string) !== -1);

export const filterBySerieGenre = (seriesList, genreId) =>
  seriesList.filter((s) => s.genre_ids.includes(genreId));


  export const filterByActorName = (actors, searchQuery) => {
    const lowerCaseSearchQuery = searchQuery.toLowerCase().trim();
    return actors.filter((actor) =>
      actor.name.toLowerCase().includes(lowerCaseSearchQuery)
    );
  };